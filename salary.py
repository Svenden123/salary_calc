salary = int(input('Введите заработанную плату в месяц: '))
mortgage_percent = int(input('Введите сколько процентов уходит на ипотеку: '))
provision_percent = int(input('Введите сколько процентов уходит на жизнь: '))
awards_in_year = int(input('Введите количество премий за год: '))

mortgage_summ = salary*0.12*mortgage_percent
provision_summ = awards_in_year*0.5*salary+salary*0.12*(100-mortgage_percent-provision_percent)

print('\n')
print('За год:')
print('На ипотеку было потрачено: %d рублей' % mortgage_summ)
print('Было накоплено: %d рублей' % provision_summ)